﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlenton
{
    public class Singlenton
    {
        private static Singlenton instance = null;
        public string mensaje = "";
        protected Singlenton() {
            mensaje = "Hola mundo";
        }
        
        public static Singlenton Instance
        {
            get
            {
                if (instance == null)
                    instance = new Singlenton();
                return instance;
            }
        }
    }
}