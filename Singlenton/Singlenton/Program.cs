﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singlenton
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Singlenton.Instance.mensaje);
            Singlenton.Instance.mensaje = "Hola marte";
            Console.WriteLine(Singlenton.Instance.mensaje);
        }
    }//
}//